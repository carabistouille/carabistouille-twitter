const Twitter: any  = require ('twitter')
const gis = require('g-i-s')
let download = require('download-file')
const fs = require('fs');
import { generateInsult, generateInsultFull, Insult } from 'carabistouille-lib'

const SECOND: number = 1000
const MINUTE: number = SECOND*60

let client:any = null
	client = new Twitter({
		consumer_key: process.env.TWITTER_CONSUMER_KEY,
		consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
		access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
		access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
	})

function connect(): void
{

	client.stream('statuses/filter', { track: 'robienTeDeteste' }, (stream: any): void =>
	{

		stream.on('error', function(error:any)
		{
	    console.log('error : ' + error)
	  })

		stream.on('data', function (tweet: any)
		{
			if (tweet.user.screen_name != "robienTeDeteste")
			{
				var statement = "@" + tweet.user.screen_name + " " + generateInsult()
				var tweetToReplyId = "" + tweet.id_str
				client.post('statuses/update', { status: statement, in_reply_to_status_id: tweetToReplyId}, function(err:any, reply:any)
				{
			  })
			}
		})

	})
}

function uploadImageWithGoogleSearch(textShort: string, cb: (id: string) => void): void
{
	let mediaId: string = ""
	if (textShort)
	{
		gis(textShort, (error:string, results:[any])=>
		{
			if (error)
			{
				console.log(error);
				cb("")
			}
			else
			{

				let uploadMedia: (url:string, cb: (id: string) => void) => void = (url:string): void =>
				{
					download(url, {filename: 'tmp'}, (err: string): void =>
					{
						if (err)
						{
							cb("")
						}
						else
						{
							var data = require('fs').readFileSync('tmp');
							client.post('media/upload', {media: data}, function(error:string, media:any, response:any)
							{
								fs.unlink('tmp')
								if (error)
								{
									cb("")
								}
								else
								{
										cb (media.media_id_string)
								}
							})
						}
					})
				}

				let i = 0
				let uploadMediaCB: (id: string) => void = (id: string): void =>
				{
					if (id == "")
					{
						i++
						if (i < 10 && results.length > i && results[i].url)
						{
							uploadMedia(results[i].url, uploadMediaCB)
						}
						else
						{
							cb("")
						}
					}
					else
					{
						cb(id)
					}
				}
				uploadMedia(results[i].url, uploadMediaCB)

			}
		})
	}

}

function tweetWithMedia(text: string, cb: (err: string) => void, mediaId?: string): void
{
	let status: any = {}
	if (mediaId && mediaId.length > 0)
	{
		status = {status: text, media_ids: mediaId}
	}
	else
	{
		status = {status: text}

	}
	client.post('statuses/update', status,  function(error:any, tweet:any, response:any)
  {
 	 if(error)
 	 {
 		 cb(JSON.stringify(error))
 	 }
 	 else
 	 {
 		 cb("")
 	 }
  })
}

function tweet(text: string, cb: (err: string) => void, textShort?: string): void
{
	if (textShort)
	{
		uploadImageWithGoogleSearch(textShort, (id: string) =>
		{
			tweetWithMedia(text, cb, id)
		})
	}
	else
	{
		tweetWithMedia(text, cb)
	}

}

function insultRegularyEverybody(delay: number): void
{
	setTimeout(()=>{insultRegularyEverybody(delay)}, delay)
	let insult: Insult = generateInsultFull()
	tweet(insult.long, (err: string): void =>
	{
		if (err === "")
		{
			console.log("tweeted : " + insult.long)
		}
		else
		{
			console.log("error : " + err)
		}
	},
 	insult.short)

}

export function run(): void
{
	connect()
	insultRegularyEverybody(20*MINUTE)

}

run()
